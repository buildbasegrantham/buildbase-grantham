For all your building materials, timber & DIY needs. 
When you trade with Buildbase, you’ll get everything you need in one place. We take pride in serving the local tradespeople and our communities, working with our customers to get the job done, so you can rely on us to give a brilliant service.

Address: S Parade, Grantham NG31 6DD, United Kingdom

Phone: +44 1476 574444

Website: https://www.buildbase.co.uk/storefinder/store/Grantham-1315

